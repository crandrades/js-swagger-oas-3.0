# Contributing
Contribution guide to create or update API contract definitions and apply 
changes to the core of this tool.

## Swagger Specs Conventions
* Paths definitions must be separated from models and placed in files 
following the patterns: 
  * **YOUR_NAME.path.yml**
* Models definitions must separated from paths and placed in files within 
the **models** folder following the patterns:
  * **YOUR_NAME.d.yml**;
  * One model per file is preferred;
  * Common models should be placed in folder **shared**.
* Tags should be placed in **tags.yml**
* All Tags must contain a description
* Contract definitions must modularized per domain (we recommend you to use tags). Eg.: 
  * clients
    * clients.path.yml
    * models
      * list_clients_request.d.yml
      * list_clients_response.d.yml
* If an endpoint contains error codes that must be handled by client applications, 
these error codes must be placed in paths definitions. 
* An endpoint can be contained in more than one group/tag, if this makes sense 
from a UX perspective and for the people who will use it
* All error codes documented must come with a descriptive message. **IT IS NOT REQUIRED** 
to put the customized error message in Swagger.
