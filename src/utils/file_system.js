/* eslint-disable global-require */

'use strict'

const FileSystem = require('fs')
const { check, flatten } = require('nodejs-fx')

module.exports.readDirRecursiveAndSync = (root, fileFilter, acc = []) =>
  flatten(
    FileSystem
      .readdirSync(root)
      .map(directory => `${root}/${directory}`)
      .map(file =>
        check(file)
          .on(isDirectory, x => acc.concat(exports.readDirRecursiveAndSync(x, fileFilter, acc)))
          .on(fileFilter, x => acc.concat(FileSystem.readFileSync(x)))
          .otherwise(() => acc))
      .filter(notEmpty)
  )

const isDirectory = value =>
  check(value)
    .map(FileSystem.statSync)
    .on(stat => stat && stat.isDirectory(), () => true)
    .otherwise(() => false)

const notEmpty = arr => arr && arr.length > 0
