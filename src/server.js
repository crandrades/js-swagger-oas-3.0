'use strict'

const Express = require('express')
const SwaggerTools = require('oas-tools')
const Config = require('./config')

const app = Express()
const swagger_config = {
  checkControllers: false,
  strict: true,
  router: false,
  validator: true,
  docs: {
    apiDocs: '/api-docs',
    apiDocsPrefix: '',
    swaggerUi: '/',
    swaggerUiPrefix: ''
  },
  oasSecurity: false,
  oasAuth: false
}

module.exports.startServerWith = specs => {
  SwaggerTools.configure(swagger_config)
  SwaggerTools.initializeMiddleware(specs, app, middleware => {

    console.debug(`\x1b[32m
 ____
/ ___|_      ____ _  __ _  __ _  ___ _ __
\\___ \\ \\ /\\ / / _\` |/ _\` |/ _\` |/ _ \\ '__|
 ___) \\ V  V / (_| | (_| | (_| |  __/ |
|____/ \\_/\\_/ \\__,_|\\__, |\\__, |\\___|_|
                    |___/ |___/                                
  `
    )

    app.listen(Config.server.port, () =>
      console.log(
        `Swagger documentation is being served on: http://localhost:${Config.server.port}`))
  })
}
