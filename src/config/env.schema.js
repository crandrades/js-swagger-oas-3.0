'use strict'

const Joi = require('joi')

module.exports = Joi.object(
  {
    PORT: Joi.string().default(3000).description('Server port'),
    SWAGGER_MODE: Joi.string().default('').description('Swagger server mode')
  })
  .unknown(true)
  .options({ abortEarly: false })
