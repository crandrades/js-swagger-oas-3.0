'use strict'

const Joi = require('joi')
const EnvSchema = require('./env.schema')
const EnvVars = Joi.attempt(process.env, EnvSchema)

module.exports = {
  server: {
    port: EnvVars.PORT
  },
  swagger: {
    mode: EnvVars.SWAGGER_MODE.trim()
  }
}
