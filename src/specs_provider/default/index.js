'use strict'

const Path = require('path')
const FileSystem = require('fs')
const YmlToJson = require('js-yaml')
const { check, peek } = require('nodejs-fx')
const { readDirRecursiveAndSync } = require('../../utils/file_system')

const BASE_SPECS_PATH = Path.resolve('./swagger_specs/default/')
const CHARSET = 'utf8'

module.exports = () =>
  check(baseSwagger())
    .map(swagger => ({ ...swagger, ...tags(), paths: paths(), components: components() }))
    .map(peek(data => FileSystem.writeFileSync(Path.resolve('__temp__/temp_swagger.json'), JSON.stringify(data, 2, 2))))
    .fold()

const baseSwagger = () =>
  YmlToJson.safeLoad(readFile('base_swagger.yml'))

const components = () =>
  check(schemas()).map(schemas => ({ schemas: schemas })).fold()

const schemas = () =>
  check(readDirRecursiveAndSync(BASE_SPECS_PATH, file => file.indexOf('.d.yml') >= 0))
    .map(Buffer.concat)
    .map(YmlToJson.safeLoad)
    .fold()

const paths = () =>
  check(readDirRecursiveAndSync(BASE_SPECS_PATH, file => file.indexOf('.path.yml') >= 0))
    .map(Buffer.concat)
    .map(YmlToJson.safeLoad)
    .fold()

const tags = () =>
  YmlToJson.safeLoad(readFile('tags.yml'))

const readFile = file =>
  FileSystem.readFileSync(Path.join(BASE_SPECS_PATH, file), CHARSET)
