'use strict'

const DefaultProvider = require('./default')
const v1Provider = require('./v1')

const ProviderMap = new Map()
  .set('v1', v1Provider)

module.exports.specsProviderFor = mode => ProviderMap.get(mode) || DefaultProvider
