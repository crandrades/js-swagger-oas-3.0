'use strict'

const FileSystem = require('fs')
const Path = require('path')

const tempDir = Path.resolve('__temp__')

/**
 * Removes Google Fonts in case that the server will run in a machine without internet connection
 */
module.exports.eliminateGoogleFonts = () => {
  const googleFontsTag = '<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Source+Code+Pro:300,600|Titillium+Web:400,600,700" rel="stylesheet">';
  const path = Path.resolve('node_modules/oas-tools/swagger-ui/index.html')
  const content = FileSystem.readFileSync(path);

  FileSystem.writeFileSync(path, content.toString().replace(googleFontsTag, ''))
}

module.exports.createTempDir = () => FileSystem.existsSync(tempDir) || FileSystem.mkdirSync(tempDir)