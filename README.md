# API DOCS
This repository enable teams to share a swagger server which holds the 
API documantation for an integration project.


## Requirements
* [Node.js][nodejs] (and [NPM][npmjs])

## Configurations
For Node.js, it's recommended to use a version management like [NVM][nvm]. 
Install latest [Node.js][nodejs] LTS version or follow the version specified 
in __.nvmrc__.

The server uses two variables to run. Those are __Server port__ and 
__Server mode__.

1. Server port: Is the port that the server will use to run. You can configure it as
an environment variable called __PORT__.

2. Server mode: Is the mode that will start the swagger server. You can configure it as
an environment variable called __SWAGGER_MODE__. This mode defines what spec provider
will be used to read the YAML files with the API documentation (for more details, refer 
to [the running section](#-running).

You can place an __.env__ file in project root to set these environment variables and 
change some configurations like swagger server port.
On application start, the server will read this and load all configurations 
as environment variables, then the server will use them.
Check [__env.sample__](./env.sample).

## Installing

If you choose to run Node.js locally, first you must install all required 
dependencies contained in [__package.json__](./package.json).
Just run: 
```sh
npm install --production
```
If you want to keep adding more documentation as a develop environment, then
install all required and dev dependencies.
Just run:
```sh
npm install
```

## Running
_Note how the server uses an environment variable called **SWAGGER_MODE** to select 
the mode of the API documentation to be shown in the swagger UI. In this example 
codebase, we use this **SWAGGER_MODE** to hold different versions of the API documentation 
but it can be used for several others mathers like environments, applications, 
infraestructure servers, and anything else._

You have several ways to run the swagger server and you can see them in the 
[__package.json__](./package.json) file.
1. If you want to run the server letting it to start the default **SWAGGER_MODE**, then 
just run:
```sh
npm run start
```

2. If you want to run the server declaring the __v1__ as the specific **SWAGGER_MODE**, 
then just run:

For unix based OS:
```sh
npm run start:unix:v1
```
For windows based OS:
```sh
npm run start:win:v1
```

3. If you want to run the server as __dev__, declaring the __v1__ as the specific **SWAGGER_MODE**, 
then just run:

For unix based OS:
```sh
npm run start:unix:v1:dev
```
For windows based OS:
```sh
npm run start:win:v1:dev
```

---

[nodejs]: https://nodejs.org/es/
[npmjs]: https://www.npmjs.com/
[nvm]: https://github.com/nvm-sh/nvm