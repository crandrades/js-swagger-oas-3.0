'use strict'

require('dotenv').config()

const Fixer = require('./src/fixer')
const Config = require('./src/config')
const { specsProviderFor } = require('./src/specs_provider')
const { startServerWith } = require('./src/server')

Fixer.eliminateGoogleFonts()
Fixer.createTempDir()

startServerWith(specsProviderFor(Config.swagger.mode)())
